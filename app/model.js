/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for rendering presentation grids
*/


//Model.js 

function  mspSolutionOutput(data){
        $("#jsGridSolution").jsGrid({
        width: "100%",
        height: "600px",
        sorting: true,
        paging: true,
        autoload: true,
 
        data: data,
        
        fields: [
            { name: "id", css: "hide", width: 0 },
            { name: "org", title:"Organization", type: "text", width: 25 },
            { name: "name", title:"Name", type: "text", width: 30 },
            { name: "solutionType", title:"Solution Type", type: "text", width: 20 },
            { name: "status", title:"Status", type: "text", width: 25 },
            { name: "lastRunTime", title:"Last Run", type: "text", width: 30 }
           ]
    });

}

function mspAgentOutput(data){
        $("#jsGridAgent").jsGrid({
        width: "100%",
        height: "600px",
        sorting: true,
        paging: true,
        autoload: true,
       
        data: data,
        
        fields: [
            { name: "org", title:"Organization", type: "text", width: 40 },
            { name: "name", title:"Name", type: "text", width: 45 },
            { name: "machine", title:"Machine", type: "text", width: 30 },
            { name: "status", title:"Status", type: "text", width: 25 },
            { name: "lastContact", title:"Last Contact", type: "text", width: 30 }
           ]
    });

}

function mspHistoriesOutput(data){
        $('#divFatalErrors').hide(); 
        $('#divRecordErrors').hide(); 
        //Clear the RECORDS GRID
        $("#jsGridErrorRecords").jsGrid({data: []}); 
        //Bind the Data Grid with the passed in array
        $("#jsGridErrorHistories").jsGrid({
        width: "100%",
        height: "250px",
        sorting: true,
        paging: true,
        autoload: true,
       
        data: data,

         rowClick: function(e) {
            //Gain Access to the ORG and Solution ID and Spin up a Modal 
            if (e.item.result == "FatalError"){
                //Make the #divFatalErrors and #divRecordErrors visible and output records
                //alert("FatalError");
                $('#divFatalErrors').show(); 
                $('#divRecordErrors').show(); 
                mspFatalsOutput(e);
                focusRecords(e);
            } else {
                //Make the #divFatalErrors not visible and market the #divRecordErrors visible
                $('#divFatalErrors').hide(); 
                $('#divRecordErrors').show(); 
                focusRecords(e);
            }
           
              
            },
       
        fields: [
            { name: "orgId", css: "hide", width: 0 },
            { name: "solutionId", type: "text",css: "hide", width: 20 },
            { name: "historyId", type: "text",css: "hide", width: 20 },
            { name: "solution", title:"Solution", type: "text", width:60},
            { name: "recordsFailed", title:"Records Failed", type: "text", width: 20 },
            { name: "result", title:"Result", type: "text", width: 20 },
            { name: "start", title:"Start", type: "text", width: 20 }
           ]
    });

    //$("#jsGridErrorRecords").jsGrid("sort", "start");

}

function mspRecordsOutput(data){
        $("#jsGridErrorRecords").jsGrid({
        width: "100%",
        height: "600px",
        sorting: true,
        paging: true,
        autoload: true,
       
        data: data,

         fields: [
            { name: "mapName", title:"Name",type: "text", width: 20 },
            { name: "sourceEntity", title:"Source",type: "text", width: 20 },
            { name: "errorMessage", title:"Error Message", type: "text", width: 30 },
            { name: "errorDetail", title:"Error Detail", type: "textarea", width: 60 },
            { name: "additonalErrorDetails", title:"Additional Error Detail", type: "textarea", width: 60 }
           ]
    });

}
function mspFatalsOutput(e){
        var fatal =[]; 
        fatal.push({result: e.item.result, details: e.item.details})
        


        $("#jsGridErrorFatals").jsGrid({
        width: "100%",
        height: "250px",
        sorting: true,
        paging: true,
        autoload: true,
       
        data: fatal,

         fields: [
            { name: "result", title: 'Result', type: "text", width: 20 },
            { name: "details", title: 'Details',type: "text", width: 80 }
           ]
    });

}


function  mspFocusOutput(data){
    //Clear the Sub Grids
    $("#jsGridErrorHistories").jsGrid({data: []});
    $("#jsGridErrorRecords").jsGrid({data: []});   
    //Load the Error Solutions Grid with the passed in data array
    $("#jsGridErrorSolutions").jsGrid({
        width: "100%",
        height: "250px",
        sorting: true,
        paging: true,
        autoload: true,
        
        data: data,
            rowClick: function(e) {
               focusHistory(e); 
            },

        fields: [
            {name: "orgName", title: "Organization", type: "text", width: 30 },
            { name: "name", title: "Name",type: "text", width: 25 },
            { name: "status", title: "Status", type: "text", width: 25 },
            { name: "solutionId",  css: "hide", width: 0 },
            { name: "orgId", css: "hide", width: 0}
           ]
    });

}

function clearGrid(gridName){
     $(gridName).jsGrid({data: [] });
    
    } 


