## Terms and Conditions ##

By using these, you acknowledege the following:

This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants you a nonexclusive copyright license to use all programming code examples from which you can generate similar functionality tailored to your own specific needs.  

These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for a particular purpose are expressly disclaimed.

Licence agreement can be found here: https://success.scribesoft.com/s/article/ka632000000GmxZAAS/Scribe-Software-Tool-Kit-And-Technology-License-Agreement

## Reference Application Example 301 - Managed Services Console ##

This simple Reference Application is written in jQuery and uses Bootstrap to provide a working example of interaction with the Scribe Online Web API to Monitor Agents, Solutions and Focus on specific executions.  More details about this can be found at https://dev.scribesoft.com/en/sample_list.htm solutions.

